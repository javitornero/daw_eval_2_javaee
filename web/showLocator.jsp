<%-- 
    Document   : showLocator
    Created on : 28-dic-2016, 22:20:05
    Author     : Javi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="locator" scope="request" class="packageData.Locator" />
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
  </head>
  <body>
    <h1>The locator is:</h1>
    Id: <jsp:getProperty name="locator" property="id" /><br>
    Name: <jsp:getProperty name="locator" property="name" /><br>
    Description: <jsp:getProperty name="locator" property="des" /><br>
    Alias: <jsp:getProperty name="locator" property="alias" /><br>
    <br>
    <a href="index.jsp">Go to Index</a>
  </body>
</html>
