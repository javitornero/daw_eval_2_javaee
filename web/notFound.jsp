<%-- 
    Document   : notFound
    Created on : 14-ene-2017, 19:31:39
    Author     : Javi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
  </head>
  <body>
    <h1>The locator has not found in the database!</h1>
    <br>
    <a href="index.jsp">Go to Index</a>
  </body>
</html>
