<%-- 
    Document   : index
    Created on : 28-dic-2016, 20:42:25
    Author     : Javi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
  </head>
  <body>
    <form action="Insert" method="post">
      <h2>Insert the locator to insert: </h2>
      Id: <input type="text" name="id"  /><br>
      Name: <input type="text" name="name"  /><br>
      Desc: <input type="text" name="des"  /><br>
      Alias: <input type="text" name="alias"  /><br>
      <input type="submit" value="Insert" /><br><br>
    </form>
    <form action="Delete" method="post">
      <h2>Delete locator by Id: </h2>
      Id: <input type="text" name="id"  /><br>
      <input type="submit" value="Delete" /><br><br>
    </form>
    <form action="GetById" method="post">
      <h2>Find locator by Id: </h2>
      Id: <input type="text" name="id" /><br>
      <input type="submit" value="Find" /><br><br>
    </form>
    <form action="GetByAlias" method="post">
      <h2>Find locator by Alias: </h2>
      Id: <input type="text" name="alias"  /><br>
      <input type="submit" value="Find" /><br><br>
    </form>
    <form action="Update" method="post">
      <h2>Update locator by Id: </h2>
      Id: <input type="text" name="id"  /><br>
      Name: <input type="text" name="name"  /><br>
      Desc: <input type="text" name="des"  /><br>
      Alias: <input type="text" name="alias"  /><br>
      <input type="submit" value="Update" /><br>
    </form>
  </body>
</html>
