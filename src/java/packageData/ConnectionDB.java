/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageData;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * This class manage the connection through a DataSource
 * @author Javi
 * @version 1
 */
public class ConnectionDB {
    /**
     * @return the con The object in charge of the connection.
     * @throws java.lang.Exception There was impossible to connect
     */
    public Connection OpenConnection() throws Exception{
        
        Connection con;
        
        try{
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/Locator");
            con = ds.getConnection();
            
            return con;
        }catch (SQLException | NamingException e){
            throw new Exception("There was impossible to connect: " + e.getMessage());
        }
    }
    
    /**
     * @param con The object to set null.
     * @throws java.lang.Exception There was impossible to close the connection
     */
    public void CloseConnection(Connection con) throws Exception{
        try{
            if(con != null)
                con.close(); // Close the connection
        }catch (SQLException e){
            throw new Exception("There was impossible to close the connection: " + e.getMessage());
        }
    }
}
