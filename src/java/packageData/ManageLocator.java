/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * We can consider this class like the controller of the application
 * @author Javi
 * @version 1
 */
public class ManageLocator {
    
    /**
     * Update a locator from the locator table of locators database
     * @param con The object in charge of the connection.
     * @param locator The object locator
     * @throws java.lang.Exception There was a problem to update the locator
     */
    public void update(Connection con, Locator locator) throws Exception{
        PreparedStatement st = null;
        
        try{
            st = con.prepareStatement("update locator set name=?, des=?, alias=? where id=?");
            
            st.setString(1, locator.getName());
            st.setString(2, locator.getDes());
            st.setString(3, locator.getAlias());
            st.setInt(4, locator.getId());
            
            st.executeUpdate();
            
        }catch (SQLException e){
            throw new Exception("There was a problem to update the locator " + e.getMessage());
        }
        finally{
            if(st != null)
                st.close();
        }
    }
    
    /**
     * Delete a locator from the locator table of locators database
     * @param con The object in charge of the connection.
     * @param locator The object locator
     * @throws java.lang.Exception There was a problem to delete the locator
     */
    public void delete(Connection con, Locator locator) throws Exception{
        PreparedStatement st = null;
        
        try{
            st = con.prepareStatement("delete from locator where id=?");
            
            st.setInt(1, locator.getId());
            
            st.executeUpdate();
        }catch (SQLException e){
            throw new Exception("There was a problem to delete the locator " + e.getMessage());
        }finally{
            if(st != null)
                st.close();
        }
    }
    
    /**
     * Insert a locator into the locator table of locators database
     * @param con The object in charge of the connection.
     * @param locator The object locator
     * @throws java.lang.Exception There was a problem to insert the locator
     */
    public void insert (Connection con, Locator locator) throws Exception {
        PreparedStatement st = null;
        
        try{
            st = con.prepareStatement("insert into locator(id, name, des, alias) values(?, ?, ?, ?)");
            
            st.setInt(1, locator.getId());
            st.setString(2, locator.getName());
            st.setString(3, locator.getDes());
            st.setString(4, locator.getAlias());
            
            st.executeUpdate();
            
        }catch (SQLException e){
            throw new Exception("There was a problem to insert the locator " + e.getMessage());
        }finally{
            if(st != null)
                st.close();
        }
    }
    
    /**
     * Get a locator from the locator table of locators database where the id match with the parameter given
     * @param con The object in charge of the connection.
     * @param id The id of the locator to find out
     * @return locator The object locator
     * @throws java.lang.Exception There was a problem to get the locator
     */
    public Locator GetById(Connection con, int id) throws Exception{
        Locator locator = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            st = con.prepareStatement("select * from locator where id= ?");
            st.setInt(1, id);
            rs = st.executeQuery();
            while(rs.next()){
                locator = new Locator();
                locator.setId(rs.getInt("id"));
                locator.setName(rs.getString("name"));
                locator.setDes(rs.getString("des"));
                locator.setAlias(rs.getString("alias"));
            }
        } catch (SQLException e){
            throw new Exception("There was a problem to get the locator " + e.getMessage());
        }finally{
            if(rs != null)
                rs.close();
        }
        
        return locator;
    }
    
    /**
     * Get a locator from the locator table of locators database where the alias match with the parameter given
     * @param con The object in charge of the connection.
     * @param alias The alias of the locator to find out
     * @return locator The object locator
     * @throws java.lang.Exception There was a problem to get the locator
     */
    public Locator GetByAlias(Connection con, String alias) throws Exception{
        Locator locator = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            st = con.prepareStatement("select * from locator where alias= ? ");
            st.setString(1, alias);
            rs = st.executeQuery();
            while(rs.next()){
                locator = new Locator();
                locator.setId(rs.getInt("id"));
                locator.setName(rs.getString("name"));
                locator.setDes(rs.getString("des"));
                locator.setAlias(rs.getString("alias"));
            }
        } catch (SQLException e){
            throw new Exception("There was a problem to get the locator " + e.getMessage());
        }finally{
            if(rs != null)
                rs.close();
        }
        
        return locator;
    }    
}
